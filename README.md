# lambdagg.xyz
my personal website hosted with GitLab Pages, available at [lambdagg.xyz](https://lambdagg.xyz).

<p align="center">
  <img src="https://lambdagg.xyz/assets/images/clippit.gif" height="240px">
  <img src="https://lambdagg.xyz/assets/images/maracas.gif" height="240px">
</p>
